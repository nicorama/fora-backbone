Backbone
====

Programme
----

* Les principes de Backbone
* Le Model
* La View
* Les Events
* Les Collections
* Javascript avancé
* Le Router
* Customiser Backbone
* Tester son application
* L'ecosystème Javascript


Introduction
====

Le MV* sur le Client : pourquoi?
----

* JavaScript et Ajax accessible pour tout : jQuery, Mootools, Prototype, etc...
* OK pour des simples animations et interactions
* KO pour des _Web Application_ très riche en fonctionnalité (SPA)
* Besoin :
	* Structurer le code et les fichiers
	* Séparer le JavaScript avec le DOM
	* Modulaire
	* Maintenable et évoluable
	
Les Framework Javascript
----

 Un choix parmis tant d'autres :

* AngularJS 1 & 2
* React
* Ember
* Knockout
* Meteor
* Spine
* Reactor

Backbone : _As close to the DOM as possible_

	
Backbone
----

* Framework pour architecturer le Front-End au style MVC
* Ce n'est pas du vrai MVC
	- Controller -> BackboneView
	- View -> Template
* Permet d'avoir un code maintenable
* Backbone est un 'backbone'. Il faut ajouter d'autres composants

### Avantages

Les Avantages de Backbone :

* Simple à apprendre
* Tout petit code (6.5ko)
* Code source très compréhensible
* On comprend ce que l'on fait
* Extrèmement souple
* Undescore est très addictif
* Se marie très bien à CoffeeScript
* Un problème, des solutions


### Défauts

Quelque défauts :

* Peu de feature
* On code beaucoup
* Dépendance à Underscore
* Se marie très mal à ES6, Typedscript

Et surtout :

!important : Ce n'est pas un Framework
	
	
Models, Views & Controller
----

* Model : 
	* donnée avec ses propriétés seulement (_get_ et _set_)
	* créer, valider, supprimer et événement
* Views : UI (affichage _model_ ou _collection_)
* Controller : 
	* manipulation des données
	* routage des urls (RESTFull)

Structure d'une Application Backbone
----

![](./images/converter.png)

### Le template

		<input type="text" />
		<select>
		    {{#units}}
		        <option>{{this}}</option>
		    {{/units}}
		</select>

		=> {{result}}
		<select>
		    {{#units}}
		    <option>{{this}}</option>
		    {{/units}}
		</select>
		<input type="button" value="OK"/>
			
![](./images/template-converter.png)


### Le Model

Quelles sont les données affichées à l'écran ?

		var ConvertModel = Backbone.Model.extend({});
		var model = new ConvertModel({
			input : 0,
			inputUnits : ["km", "m", "mm", "miles", "yards", "feet"],
			outputUnits : ["km", "m", "mm", "miles", "yards", "feet"],
			result:null
		});

### Le Controller (Backbone.View)

Le controller définit les actions possibles :

* convert : clique sur OK
* chooseOutputUnits : on supprime une Unit du combo

<br/>
	
 		var ConverterView = Backbone.View.extend({
            initialize : function(){},
            convert : function(){},
            chooseOutputUnits : function(){},
            render : function(){}
        })


### Les Events

* La gestion des evenements est simple et riche
* Evénments prédéfinis
* Evénements personalisés
* Bus d'événement custom

### Event bus et événements custom


        myBus = _.extend({}, Backbone.Events)

        myBus.on('foo:bar', function (){
           alert('this was a foo:bar alert !');
        });

        myBus.trigger('foo:bar');


### Event prédéfinis


        //same code

        var myModel = new Backbone.Model({
            foo:'not cool value'
        });

        myModel.listenTo(myBus, 'foo:bar', function (data){
            console.log('who is this ?', this);
            console.log('what data ?', data);
            this.set('foo', data);
        });

        myBus.trigger('foo:bar', "some data");


TP
---

* Construire le converter
* Modifier automatiquement les outputUnits

Underscore
====

Introduction
----

* Underscore est une bibliothèque **utilitaire**
* Programmation fonctionnelle sur JavaScript
* Facilite la vie sur JavaScript : ne réinventons pas la roue 
* **Backbone** depend d'**Underscore**
	- Autant donc bien savoir l'utiliser
	
Underscore : programmation fonctionnelle
-----

JavaScript pur : 

		for (var i = 0; i < comments.length; i++){
			var comment = comments[i];
			checkContent(comment);
		}


Avec Underscore :

		_(comments).each(checkContent);


Underscore : caractéristique
----

* Code plus court
* Code beaucoup plus lisible

### Exemple

		_.each(comments, function(comment, index) {
			var user = getCommentUser(comment);
			if(user)
				featureUser(comment, user);
		});

Note : forEach est aujourd'hui implémenté sur les browsers :

        var x = [1,1,3,9,7,6];
        x.forEach(function(x, index){
            console.log(x,index)
        });

Dans ce cas Underscore delegue à l'implementation du browser

Underscore : réutilisation de code
----


		function compareByDate(com1, com2) {
			return com1.dateCreation <= com2.dateCreation;
		}

		_.sortBy(comments, compareByDate);


Underscore : enchaînement des instructions
----

* Eviter les codes très longs pour des opérations complexes
* Ecrire et tester les fonctions individuellement

<br/>

		_.filter(comments, notAnonymous)
			.sortBy(compareByDate)
			.each(featureUser);


Underscore : les fonctions les plus utilisées
----

`each(list, iterator)` : parcourir une liste et faire une action pour chaque élément

		_.each([com1,com2,com3],showRate);

`map(list, iterator)` : faire correspondre une liste à une autre liste

		var cleanComments = _.map([com1, com2, com3], function(com) {
			return checkContent(com);
		})

`find`, `filter`, `pluck` : Filtrage

		var anonymousComments = _.find([com1, com2, com3], function (com){
			return com.isAnonymous;	
		});

		var users = _([com1, com2, com3]).pluck(function (com){
			return com.user;	
		});->["john", "jim", "jane"]


Underscore : les fonctions les plus utilisées (suite...)
----

Travailler avec les objets :

* `_.isObject(o)`
* `_.isNumber("3")` : NaN est un nombre...
* `_.isArray(o)`
* `_.isNull(o)`
* etc.

### beaucoup d'autres fonctions

* `_.union(arr1, arr2)`
* `_.intersection(arr1, arr2)`
* `_.différence(arr1, arr2)`
* `_.uniq(arr1, arr2)`



### Bind it like Underscore


* `_.bind(obj)` permet de lier le `this` d'une fonction à `obj`
* Très utile pour les callbacks
* Utiliser en profondeur par Backbone

<br/>


        var func = function(greeting){ return greeting + ': ' + this.name };
        func = _.bind(func, {name: 'moe'});
        var result = func("Hello");

        console.log(result);// Hello moe


### Backbone bind function

* Le 3e argument de bind permet de préciser `this`

        MyView = Backbone.View.extend({
          initialize: function(){
            this.model.bind("change", this.render, this);
            //will call
          },
          render: function(){ ... }

        });


Backbone Model
====

Introduction
----

* Définit le Model du MVC
* Represente les données de l'application pour un MVC
* Ou les données d'une Vue pour un MVP

### Créer un model Backbone :

* utiliser la fonction `Backbone.Model.extend()`
* l'objet `default` pour preciser les valeurs par défaut
* la fonction `initialize(attributs)` est appellé automatiquement
	- certains attributs sont spéciales (id, el, collection...)
	- les autres sont lisibles par `model.get("attr")`
* on peut ensuite enrichir le Modèle par d'autres fonctions 
	
Backbone Model : exemple
----


		var Comment = Backbone.Model.extend({
			//valeur par défaut
			defaults: {
				comment: "Commentaire vide",
				isAnonymous: true
			},
			//valeur par défaut si le commentaire est vide
			initialize: function() {
				if(!this.get("comment")){

					this.set({"comment": this.defaults.comment});
				}
			},
			//supprimer un commentaire
			clear: function() {
				this.destroy();
				this.view.remove();
			}
		});


Backbone Model : les propriétés
----


On passe un objet JSON en parametre :

		var myComment = new comment({
			'isAnonymous':true,
			'comment':'My first comment'
		});

on utilise la methode `set` :

		var myComment2 = new comment();
		myComment2.set({
			'isAnonymous':true,
			'comment':'My second comment'
		});


Backbone Model : `initialize` et `constructor` 
----

* `initialize` est appellé à l'initialization
* `constructor` n'est pas très utilisé :
	- `initialize` est appéllé à l'intérieur du `constructor`
	- Utiliser `constructor` dans des cas très particuliers (hack)

<br/>

		var Comment = Backbone.Model.extend({
		  initialize : function() {
		    //...
		  },
		});
		var AuthenticatedComment = Comment.extend({
		  constructor : function (options) {
		    Backbone.Model.prototype.constructor.call(this, options);
		    //...
		   }
		}


Backbone Model : methode `toJSON`
----

* Le Model est un objet complexe
* On veut afficher à l'ecran les données brutes

<br/>

		var entity = myComment.toJSON();
		delete entity.password
		template.render(entity)

* A l'inverse `Model.parse()` fait le travail inverse

Backbone Model : création et enregistrement - `save`
----

* Il faut simplement appeler la methode `save` si back-end RESTFull
* Il faut personnaliser si le backend n'est pas RESTFull
* Par exe. si côte serveur ne prend pas en charge les requêtes PUT et DELETE alors il faut prevoir tout de suite une personnalisation (voir `sync`)

<br/>

		myComment.save(); //http://myserver:port/comment -> verbe : POST 
		myComment2.save(); //http://myserver:port/comment -> verbe : POST

* En interne, Backbone utilise cID pour les models à crée mais les modèles qui viennent du serveur auront un ID
* Notez que Backbone utilise JSON pour la communication avec le serveur (`fetch`,`save`, etc.)

Backbone Model : suppression
----

`destroy()` supprime un modèle de la base

		myComment.destroy(); //http://myserver/comment/1 -> verbe : DELETE 
		myComment2.destroy(); //http://myserver/comment/2 -> verbe : DELETE

* `destroy()` supprime également le modèle de sa collection
* Attention, les objects écoutant ce modèle doivent réagir à `"destroy"`
 
Backbone Collection
====

Sommaire
----

* Gestion d'une liste de model du même type
* On fait une extension pour preciser les propriétés de notre `Collection`
	* `model` : type de model de la `Collection`
	* url : source des donnés
* Toutes les fonction Underscore sont alors possibles

<br/>	

		var TopicComments = BackBone.Collection.extend({
			model: Comment,
			url : "topics/"+this.id+"/comments"
		});
		var comments = new TopicComments({id:4});
		comments.fetch({
			success : function(){...}
		});


Backbone View
====

Sommaire
----


* Affichage des données de l'application sur la page HTML
* Propriétés :
	* `tagName` et `className` : type d'élément et classe du container de la vue
	* `el` : élément à l'interieur de la quelle il faut inserer  la vue
	* `events` : différents événements de la vue
	* `initialize` : appellé à l'initialization
	* `template` : stocker en cache le template à utiliser
	* `render` : aute-géneration du HTML de la vue


Backbone View : code
----


		var AppView = Backbone.View.extend({
			tagName: "article",
		    className: "contact-container",
		    template: $("#contactTemplate").html(),
			el: $("#forum"),
			events: {
				"keypress #new-comment": "sendComment",
				"click #send": "sendComment"
			},
			initialize: function(){
				
			},
		    render: function () {
		        var tmpl = _.template(this.template);
		        this.$el.html(tmpl(this.model.toJSON()));
		        return this;
		    },
			add: function(com){
				var view = new CommentView({model: com});
				this.$('#forum-comment').append(view.render().el);
			}
		});



Taxinomie
----

* Une Backbone.View est en fait un **Controller**
* La vue est le rendering de {template+data}
* On utilisera l'anglicisme **View** pour la Backbone.View
    - et *vue* ou *rendu* pour {template+data}

Création d'une Vue
---

Chargement des données : 

* Il n'y a pas d'*opinion* sur ce sujet
* On peut charger avant la création de l'objet
* Ou dédier ce travail à la View
    - la View sera donc bien un Controller

### Exemple

	   var CommentView = Backbone.View.extend({

		   load : function(render){
		        var comment = new Comment({id:1});
		        var view = this;
		        comment.fetch({
		            success : function(comment){
		                view.model = comment;
		                if (render){
		                    view.render();
		                }
		            }
		        });
		    }
		    //other methods
		});

		var commentView = new CommentView({el:$("#main")}).load(true);


### Autre solution

On charge le model en dehors de la View :

        var comment = new Comment({id:1});
        comment.fetch({
            success : function(comment){
                var commentView = new CommentView({model : comment});
                $("#main").html(commentView.render().$el);
            }
        });


### Attacher la View au DOM

Une View s'affiche en deux étapes :

* Créer le HTML de la View : render()
* Attacher ce *snippet* à un élément du DOM existant

Deux remarques :

* On peut attacher la View avant d'appeler '`render()`
* la fonction `render()` renvoit toujours la View
* `view.el` représente l'élément du DOM
* `view.$el` est un wrapper jQuery créé juste après `view.el`


### Exemple 

Création du Snippet

		var userView = new UserView();
		//Création du snippet ; render() revoit la view
		userView = userView.render();
		// Attache du snippet
        $("#main").html(userView.$el);

Attacher la View avant la création du snippet

		var commentView = new CommentView({el:$("#main")})
		$("#main").html(commentView.render().$el);
		// Attention à 'el' et '$el'


### Utiliser une collection

* On peut utiliser `collection` plutôt que `model`
* `render` va alors s'appuyer sur cette variable
* Il est facile de *binder* les modifications de la collection sur le controller

<br/>

 		if (this.collection != null){
            this.listenTo(this.collection, 'add', this.add);
            this.listenTo(this.collection, 'remove', this.remove);
            /*
        	this.listenTo(this.collection, 
            	"reset sync add remove", this.render);
            */
        }


Note : On peut  utiliser à la fois `collection` et `model`, voire collectionXYZ


Fight the Zombies
----

* Un problème récurrent est l'apparition de Zombie
* Une vue non visible reçoit des événements
* Une méthode `view.close()` est souvent necessaire

<br/>

        Backbone.View.prototype.close = function(){
          this.remove(); // Unbind all local event bindings
          this.unbind();  // Remove view from DOM
          if (this.onClose){    //allow custom implementation
              this.onClose();
          }
        }


        MyView = Backbone.View.extend({
          initialize: function(){
             this.model.bind("change", this.render, this);
          },
          render: function(){ ... },

          onClose: function(){
              this.model.unbind("change", this.render);
          }

        });


Mustache & Handlebar
====

Introduction
----

* Un moteur de template sans logique - _logic-less_ : sans `if`, `else` ou des boucles
* Moteur de template pour HTML, fichier de configuration en JSON, etc.
* Très leger

Mustache : exemple
----

HTML : 

		<h3>{{title}}</h3>
		<ul>
			{{#comments}}
			<li>{{comment}}</li>
			{{/comments}}
		</ul>

JavaScript :

		var x = {
			"title": "Commentaires",
			"comments": [
				{"isAnonymous": true, "comment": "First comment..."},	
				{"isAnonymous": true, "comment": "Second comment..."},	
				{"isAnonymous": true, "comment": "Third comment..."}	
			]
		}

HandleBars
---

* HandlerBars est une super-implémentation de Mustache
* objets liés : `{{user.adress.street}}`
* des `each`, `if/else` plus clairs
* rajouter des fonctionnalités (ex : `toLowerCase())


Compilation et performances
----


* Compiler  transforme le template en `function(data)`
* Certaines versions de Mustache ne *compile* pas le template
	* Mustache fait alors des `replace (/.../)` à la volée
* Modifier de larges templates peut prendre du temps et créer des défauts visuels

Si les performances se dégradent, on peut :

* Découper le template en composants, et faire des transformation plus fines
* Envisager d'utiliser **React.js** comme View d'un composant


Gestion des Events
====

Introduction
----

Il y a deux type d'événements :

* Evenement DOM dans un View Backbone
* Création d'un *Event bus*



Evénement DOM
----

![](images/backbone_event.png)

(1) Evénement DOM géré dans un view Backbone
(2) Selecteur CSS
(3) La fonction JavaScript à appeller

Ce type d'événement est essentiellement interne à la View. On est en mode **Component**.

Event Bus
----

* Un Event Bus permet de découpler l'application
* Plusieurs objets différents peuvent discuter entre eux
* Il s'agit du Design Pattern **Observable**
* Déclencher avec la méthode `trigger`

<br/>

	
    var eventBus = _.clone(Backbone.Events);


    eventBus.on("fire", function(people){
        alert("you are firing :" + people);
    })

    var john = new model.User({
        id : 2
    });

    john.laugh = function(){
        console.log("ah ah ah")
    }

    john.listenTo(eventBus, 'fire', john.laugh);

    eventBus.trigger("fire", "Jack")



Backbone.Router
====

Objectifs
---

* Demonstration de l'API Javascript
* Permettre l'utilisation du **hash tag**
  `myApp/#comments/12`
* Ou avec HTML5 l'api *pushState* : `myApp/comments/12`
* Peut être ajouter dans le favoris




Création du Router
-----

		var AppRouter = Backbone.Router.extend({
	        routes: {
1	            '' : 'home',
	            'forum': 'forum',
	            'all-users': 'users',
	            'flags' : 'flags',
	            'user/:id' : 'user'

	        },

	        home : function(){
	            //by default forum
	            this.forum();
	        },

	        forum  : function(){
	            console.log("Router to Forum")
	            tabs.trigger("selection", {index : 0});
	        },

	        users : function(){...},
	        user : function(id){...},
	        flags : function(){...}
	    });

	    var router = new AppRouter();
	    Backbone.history.start({pushState : true});


L'application de *met dans l'état* souhaité avec `tabs.trigger()`.

### Modification dynamique des routes

* Avec l'api hashtag, il faut mettre des balises se type `<a href="#flags">`
* Un changement d'état dans l'application se *signale* avec :
	- `router.navigate("users")`

###  PushState

On peut également utiliser l'API *pushState*

* L'api pushState est plus lisible :
	- `Backbone.history.start({pushState : true});`
	- `<a id="flags">`
* Cependant avec *pushState* :
	- le cursor 'hand' disparait sur le lien
	- il faut également que le serveur web reconnaisse l'URL


### TP

* Positionner le Router sur la TabView
* Choisir le bon utilisateur


Javascript pour Backbone
=====

Le DOM (Document Object Model)
---

#### API : lire la structure d'un document HTML/XML



		var nameElt = document.getElementById('name'),
		hdeux = document.getElementsByTagName('h2'),
		firstClassfields = node.getElementByClassName('firstField');


### Changer le contenu du HTML


		<img src='logo.png' id='logo' />

		var logo = document.getElementById('logo');
		logo.src = '/images/new_logo.png';
		//Une autre manière
		logo.setAttribute('src','/images/new_logo.png');
		document.getElementById('title').innerText = 'Nouveau logo pour notre site';

    
### Mettre à jour la structure ; querySelector

		var content = document.querySelector("#content"),
		var newContent = document.createElement('span');
		newContent.innerText = 'contenu mis à jour';
		content.appendChild(newContent);


### Changer le style ; querySelectorAll ; Underscore


		var elements = document.querySelectorAll(".element"),
		_.each(elements, function(element){
				element.className="component"
				element.style.color="blue"
			})

  
### Problème de reflow et repaint

* Une manipulation excessive du DOM impacte la performance
* Solution
	- créer la structure en dehors du DOM avant d'ajouter l'ensemble
	- C'est ce que fait view.render()

<br/>	

		var dataContainer = document.getElementsById("data");
		var fragment = document.createDocumentFragment();
		for ( var e = 0; e < elems.length; e++ ) {
			fragment.appendChild( elems[e] );
		}
		dataContainer.appendChild( fragment.cloneNode(true) );
		//un seul repaint à la fin;


Gestion des évènements
---

#### Evenement de la souris


* click
* dblclick
* mousedown
* mousemove
* mouseout
* mouseover
* mouseup

### Evenement sur un champs

* focus & blur
* change
* reset & submit pour FORM

#### Autres événements


* page : onerror, onload
* clavier : keydown, keypress, keyup

### Event handler


		<!-- Old school -->
		<div id="logo" onclick="logoOnClick">...</div>

On peut également faire la relation en Jvascript :

		var logo = document.getElementById("logo");
		var logoOnclick = function(){
		  alert('Goto home page!');
		};
		// On a du bubbling si dernier argument à true
		logo.addEventListener('click', logoOnclick, false);
		
		//'également :
		logo['onclick'] = function(){
		  alert('Goto home page!');
		};
    


### Bubbling


* l'évenement se propage à l'arbre des parents de l'element déclencheur

Quand l'utiliser?

* Vous avez beaucoup d'objet (20 cercle manipulable)
* On peut créer 20 _event handler_ pour ces objets
* On peut aussi créer un seul _event handler_ pour le containeur

### Annuler la propagation : JS pur



		var eventHandler = function (e) {
		  e = e || event;
		  var target = e.target || e.srcElement;
		  e.cancelBubble = true;
		  if(e.stopPropagation){
			e.stopPropagation();
		}


#### Annuler la propagation :  jQuery style


		$( "send" ).click(function( event ) {
		  event.stopPropagation();
		  //...
		});

jQuery nous simplifie la vie 

jQuery
----

### Utilité du Framework


* La **manipulation du DOM** est fastidieuse.

Pour faciliter la tache, on utilise un Framework :

* jQuery
* Zepto
* Dojo
* ...

Autre utilisation : **animation**, **Ajax**

### Principe de base


* Filtrer : _selectors_
* Agir sur le(s) élément(s)
	* changer le contenu
	* gerer les événements des éléments
	* changer la structure
* jQuery est-il une monade ?
	
### Syntaxe de base
	

		$('div').addClass('container');
		$('div.content1').slideUp();
		$('div.content2').slideDown();
		jQuery('div').addClass('container');
		$('#comments').plugins1(); // importance du nombre de plugins disponible


Remarque : Object jQuery => _$_ ou _jQuery_ 

### Filtre jQuery

* `$('h1')` : tous les `<H1>`
* `$('#container')` : `id = "container"`
* `$('div#container')` : `<div id="container" >`
* `$('div.container')` : `<div class="container" >`
* `$('div#container p')` : tous les `<p>` dans _container_

<br/>

		var container = $("#container");
		container.find("p");//tous les `<p>` dans _container_

L'état de l'art suggère de sélectionner uniquement à partir des classes.

### Manipulation

		$('input#age').val()
		$('input#age').val("18")

		$('div#content').append('Hello')
		$('div#content').prepend('Hello')

		$('div.desktop').remove()
		var desktop = $('div#desktop').detach()
		$('div.desktop').hide()

### Evénement 


		$(document).ready(function(){
			//Excuter le scrit après le chargement de la page
		}); 

		//old school
		$('div#container').click(function(e){
			//TODO
		}); 

		//Much Backbone style
		$('div#container').on("click", function(e){
			//TODO
		}); 

		// delegate : une fonction pour de multiples éléments
		// utilisé dans Backbone
		// all events declarations are scoped to the BackboneView's el.
		$( "div#container" ).delegate("click", "li", function() {
		  	$( this ).toggleClass("selected");
		});

  
### Ajax

* $.ajax()
* $.get()
* $.post()
* $.getJSON()

### $.ajax()

		$.ajax({
			type:"GET"
			url: "users/all",
			data: "{user:2, name:'john'}", //?user=2&name=john for GET
			success: function(data){....},
			eror: function(xhr, status, error){....},
			dataType: "json"
		});



### Ajax Promise Style
		
		//promise style
		$.post("/forum/1/comments",{
			data: { anonymous: true}
		})
		.done(function() { alert("success"); })    
    	.fail(function() { alert("error"); })    
    	.always(function() { alert("complete"); }); 


### Backbone et les promises

Backbone utilise jQuery pour faire de l'Ajax

		this.comment.save()
		.done(function() { alert("success"); })    
    	.fail(function() { alert("error"); })    
    	.always(function() { alert("complete"); }); 

Attention : `fetch` ne renvoit pas de *promise*

### Chaining



		$('div#container').css("color","blue").slideDown(); 

		//underscore utilise également le chaining







La programmation prototypée en JavaScript
----

#### Objectifs


* JavaScript n'a pas de notion de class comme on a sur Java ou C#
* Pas de _class_ -> on crée un objet à partir d'un autre
	* Plus libre et rapide avec JavaScript
* La notion d'objet et l'héritage existe (_"Prototypal inheritance"_)
* Une fonction est un objet, un tableau est un objet

### Exemple



		// _Comment_ est en _pascal case_ par convention
		// pour dire que c'est un constructeur
		function Comment(subjectId, comment){
			// propriété _Subject_ est crée ici - dynamicité
			this.SubjectId = subjectId;
			
			// propriété _Comment_ est crée
			this.Comment = comment;
		}
		var comment = new Comment(1,'First comment!');


### Prototype d'une fonction


* Toutes les fonctions ont une propriété _prototype_
* Toutes les propriétés prototypes sont ajouté à la fonction
* Chercher les propriétés/méthodes dans l'objet puis dans les prototypes après
* Performance : prototype > propriété directe

<br>

		function Comment(){};
		var c = new Comment();
		Comment.prototype.setComment = function(comment){
			this.comment = comment;
		};
		c.setComment('First comment');
		alert(c.comment);


### Augmentation d'objet



		var anonymousComment = new Comment(0,'First comment');
		anonymousComment.isAnonymous = true;
		alert(anonymousComment.SubjectId);
		//
		var authenticatedComment = new Comment(1,'Second comment');
		authenticatedComment.isAnonymous = false;
		authenticatedComment.userName = 'Robusta';
		alert(authenticatedComment.SubjectId);


### Héritage prototypé



		function Comment(){};
		Comment.prototype.subjectId = 0; 
		Comment.prototype.comment = ''; 

		function AuthenticatedComment(userName){
			this.userName = userName;
		};
		AuthenticatedComment.prototype = new Comment();
		AuthenticatedComment.prototype.isAnonymous = false;

		var comment = new AuthenticatedComment('jean');
		console.log(comment.subjectId);
		console.log(comment.comment);
		console.log(comment.userName);
		console.log(comment.isAnonymous);


### Héritage : autre cas pratique



		var options = {maxComVisible: 10, allowAnonymous: true};
		var myOptions = Object.create(options);
		myOptions.allowAnonymous = false;
		// myOptions.__proto__ == options


* Si **options** crée à partir d'un autre objet => **myOptions** peut acceder à ses membres
* Changement dans **options** se propage dans **myOptions**
* Changement dans **myOptions** n'impacte pas **myOptions**


### Le rôle de l'opérateur new


		Forum = function() {this.admin = 'nicolas';};
		// Just a function

		Forum.prototype.copyright = 'Robusta Code';
		// like all functions, Forum has an accessible prototype property 

		club = new Forum();
		// 3 things just happened
		// A new, empty object {} was created called club.
		// The [[prototype]] property of club was set to a copy of the prototype property
		// of Forum. The Forum function was executed, with club in place of "this"
		// so club.admin was set to 'nicolas'

		club.admin;// returns 'nicolas'
		club.coyright;
		// club doesn't have a property called 'copyright',
		// Its [[prototype]] is the same as Forum.prototype. NOT A COPY !
		// Forum.prototype has a property called 'copyright' with value 'Robusta Code'
		// returns 'Robusta Code'


### Parenthèses Optionnelles


		//identiques
		var club = new Forum();
		var club = new Forum;
		var comment = new Backbone.Model

		//Attention aux mauvaises habitudes. Non identiques !
		var club = Forum();
		var club = Forum;

### IntanceOf



		comment instanceof AuthenticatedComment; //true
		comment instanceof Comment; //true
		comment instanceof Object; //true

* Toute les fonctions sont à la base une prototype de _Object_

### Avec Backbone

		var Item = Backbone.Model.extend({
            className : 'Item',
            size :10
        });

        var VerySmallItem = Item.extend({
            size :0.1
        });

        var item = new Item();
        var verySmall = new VerySmallItem();

        alert("item is Model ?" + (item instanceof Backbone.Model)); //true
        alert("verySmall is Model ?" + (verySmall instanceof Backbone.Model)); 



Customiser Backbone
=====




Marionette
-----

#### Objectifs

* Très variés
* Gestion d'une application
* Quelques solutions *out of the box*

### Problème

* Beaucoup de solutions ne seront pas utilisé
* Découpage en plusieurs modules
* Compliqué de s'y retrouver
* Beaucoup de doc pour peu de code
* Difficile de faire ensuite évoluer la solution
* => s'inspirer du code source



### Application

* Permet un bootstrap clair d'une application
* Plusieurs applications possible


var MyApp = Marionette.Application.extend({
  initialize: function(options) {
    console.log(options.container);
  }
});

var myApp = new MyApp({container: '#app'});

### Custom events



        MyApp.on("start", function(options){
          if (Backbone.history) Backbone.history.start();
        });

        MyApp.on("before:start", function(options){
          options.moreData = loadI18n('de');
        });


### Views

* ItemView : affiche un élément
* Collection View : affiche un groupe d'élements
* CompositeView : affiche un arbre (branche, feuilles)
* Region : Lieux où se placent les Views

### Code

        myApp.addRegions({
          mainRegion: "#main-content",
          navigationRegion: "#navigation"
        });

        var MyChildView = Marionette.ItemView.extend({});

        Marionette.CollectionView.extend({
          childView: MyChildView
        });

        var myCollectionView = Marionette.CollectionView.extend({...});
        myApp.mainRegion.add(myCollectionView);



### Utilisation avec HandleBar

* Marionette redefini la fonction render()
    - Opiniated
* On peut remplacer `view.template` par une fonction


<br/>

        var MyView = Backbone.Marionette.ItemView.extend({
          template: Handlebars.compile($("#assign-products-main-view").html()),
          model: new Backbone.Model({name: "Steve"})
        });








Backbone sync
---

#### Fonction


* Assure la comunication avec le serveur
* Pas besoin de personnalisation si le BackEnd est RESTFull
* Permet l'ajout/modification de fonctionnalités
	- emulation de PUT/DELETE/PATCH
	- modification du content-type
	- ajout d'un header Autorization
	- sauvegarde dans le local-storage
* Modifier Sync est un projet complexe

### Exemple : Modifier les Headers


		var backboneSync = Backbone.sync;
		Backbone.sync = function (method, model, options) {

		    options.headers = {
		        'Authorization': "BASIC "+base64(username+":"+password)
		    };
		    /* Call the stored original Backbone.sync method */
		    backboneSync(method, model, options);
		};


Utiliser le local Storage
----

* Il est possible d'utiliser le local storage
* save(), delete()... sont alors redéfinis
* La sauvegarse peut être switché selon la connection du *device*
* On preferera utiliser des *guid*
* Backbone.localstorage est un [projet *officiel*](http://backbonejs.org/docs/backbone.localStorage.html)



### Redefinition des fonctions :


        // One store per Model
        Backbone.LocalStorage = window.Store = function(name) {
            this.name = name;
            var store = window.localStorage.getItem(this.name);
            this.records = (store && store.split(",")) || [];
        };

        //creating a string !
        save: function() {
            window.localStorage.setItem(this.name, this.records.join(","));
        }

        //parse the string
        find: function(model) {
            var data = window.localStorage.getItem(this.name+"-"+model.id)
            return data&& parseJson(data);
        }


### Variante

* Le localStorage n'est pas une base de donnée
* Il faut parser/jsonifier à chaque sauvegarde ou lecture
* Utilisation d'un cache
    - risque si deconnexion
* Utilisation de websql/indexedDB
    - can i use IndexdDb ?






Tester son Application
====

Principe des tests
----

Il existe plusieurs types de tests

* Tests unitaires
* Test de comportement
* Tests d'intégration
* Tests de performances

Test de comportement
----

#### Jasmine

* La différence entre UT et BT est ténue
* Les deux doivent tourner très vite
* Les BT peuvent remplacer les TU
* Jasmine a été écrit par l'équipe de JSUnit
* Angular favorise Jasmine
* Angular apporte un $httpBackend pour mocker $http

### Karma

* Karma permet de lancer automatiquement les BT
* Karma peut lancer des browsers automatiquement
	- y compris PhantomJS
* Karma s'utilise en général avec Gulp ou Grunt
* La configuration n'est pas très simple
* Karma n'est pas bon pour la planète

<br/>

		npm install -D karma
		sudo npm install -g karma-cli
        karma start


### Exemple

Unit test simple :

        it('should set the new ingredients', function() {
           coffee.setIngredients([24, 22, 22, 9, 3]);
           expect(coffee.get('ingredients')).toEqual([24, 22, 22, 9, 3]);
        });

Mock Server data :

        describe('when it fetches', function() {
           var coffee;

           beforeEach(function() {
              spyOn($, 'ajax').andCallFake(function(options) {
                 options.success(MOCK_GET_DATA);
              });
              coffee = new App.Model.Coffee();
              coffee.fetch();
           });

           // testing after app get mocks data

           afterEach(function() {
              coffee = undefined;
           });
        });




Tests end to end avec Protractor
----

* Developpé pour Angular ($watch ...)
* Protractor permet de faire des tests End to End
* Utilise le Web Driver de Selenium
* API très pratique


### SetUp


		sudo npm install -g protractor
		sudo webdriver-manager update

		webdriver-manager start
		protractor protractor.conf.js



L'ecosystème JavaScript
====

Node JS
----

* Execute du code Javascript
	- node myFile.js
	- utilise V8
* Créer un serveur d'application
	- Node
	- Connect : serveur
	- Express :framework
	- Mongoose : ORM
	- MongoDB

Gestion des dépendances
---

### NPM 

* NodeJS est packagé avec NPM
* NPM gère des modules de type CommonJS
	- `npm install myModule`
	- `require("myModule")`
* NPM regroupe les dépendances d'un projet dans `package.json`
	- `npm install --save myModule`


### Bower

* NPM gère les dépendances du serveur
* Bower gère les dépendances du client

Utilisation : 

		Install Node and npm (download .exe or .dmg)

		$ npm install # make the project npm ready ;install what is in package.json
		$ sudo npm install -g bower # install bower
		$ bower install # copy bower.json dependencies in ./bower_components
		$ npm install -g gulp # install gulp
		$ gulp build # build your project
		$ gulp serve # you're ready for it


Autres outils
---

* Plugins Backbone : tout et n'importe quoi ; s'en méfier
* CoffeeScript/TypedScript : simplifie l'écriture de Javascript
* Grunt / Gulp : automatise les tâches courantes (minification, tests)
* Jasmine, Karma : outils de tests unitaires automatique
* Selenium, Protractor : tests d'intégration
* Gtaling : tests de performances
* PhoneGap : HTML5 sur mobile
* JS Lint & JS Hint : Equivalent de checkstyle

