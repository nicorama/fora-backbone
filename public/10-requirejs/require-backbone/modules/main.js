console.log("I am in main.js directly loaded by data/main by require.js")


require(["jquery", "underscore", "backbone", "user", "text!some-text.template"], function ($, _, Backbone, user, template) {

    user.hello()
    user.bye()

    $("#content").html(template)
    _.each([1,2,3], function(elt){
        console.log (elt)
    })

    console.log(Backbone)
});
