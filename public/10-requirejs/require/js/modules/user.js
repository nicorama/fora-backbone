define([], function(){

    var User = function(name, pwd){
        this.name =name;

        //could we use prototyping here ?
        this.getName = function(){
            return this.name;
        }



    }
    var john = new User ("John");

    return {

        // Export 'class'
        User : User,

        hello : function(){
            console.log("hello from : " + john.getName());
        },

        bye : function(){
            console.log("bye from : " + john.getName());
        }

    }

})
