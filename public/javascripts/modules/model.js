define(["backbone"], function (Backbone) {



    var Topic = Backbone.Model.extend({

        url: function () {
            return "topics/" + this.get("id")
        },


        toString: function () {
            return this.get("title");
        },

        parse : function(data){
            console.log(data);

            this.comments = new CommentCollection(data.comments)
            this.user = new User(data.user);

            delete data.comments;
            delete data.user;

            console.log(data);
            return data;
        },

        getComments : function(){
            return this.comments;
        },

        getUser : function(){
            return this.user;
        }
    });


    var Comment = Backbone.Model.extend({

        defaults : {
            score : 0
        },
        url: function () {
            return "comments/" + this.get("id")
        },
        toString: function () {
            return this.get("content");
        }
    });


    var User = Backbone.Model.extend({

        initialize : function(){
            if (_.isUndefined(this.get("admin"))){
                this.set("admin", false);
            }
        },

        url: function () {
            return "user/" + this.get("id")
        },
        toString: function () {
            return this.get("name");
        },

        isAdmin: function () {
            return this.get("admin");
        }
    });


    var TopicCollection = Backbone.Collection.extend({
        model: User,
        url: "topics"
    });

    var CommentCollection = Backbone.Collection.extend({
        model: User,
        url: "comments"
    });

    var UserCollection = Backbone.Collection.extend({
        model: User,
        url: "users"
    });


    var AdminCollection = new UserCollection();

    AdminCollection.parse = function () {
        //stuff
    }

    return {
        User : User,
        Topic : Topic,
        Comment : Comment,
        UserCollection : UserCollection,
        AdminCollection : AdminCollection,
        TopicCollection : TopicCollection,
        CommentCollection : CommentCollection
    }

});