define(["backbone", "handlebars", "text!templates/comment-view.template"], function(Backbone, handlebars, tpl){

var TopicView = Backbone.View.extend({


    template : handlebars.compile(tpl),

    render : function(){

        var data = this.model.toJSON();
        this.$el.html(this.template(data));

        this.model.getComments().each(function(comment){

            var commentView = new CommentView({
                model : comment
            });
            commentView.render();
            this.$el.find(".comments").append(commentView.$el)

        })

        return this;

    }


})

});
