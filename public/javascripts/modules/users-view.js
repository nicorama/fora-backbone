define(["backbone", "handlebars", "text!templates/users-view.template"], function(Backbone, handlebars, tpl){

    var UsersView = Backbone.View.extend({

        events : {
            "click .users li, .admins li" : "changeUser",
            "click button.set-admin" : "setAdmin",
            "click button.delete" : "deleteUser",
            "click button.cancel" : "cancel"
        },

        initialize : function(options){
            this.current = options.current;
            this.time = options.time
            //collection is a Backbone Collection

            /*
            var that = this;

            this.collection.on("change:admin remove", function(model, options){
                console.log(model.get("name") + "fires for "+that.time, options)
                that.render();
            });
            */

            this.listenToOnce(this.collection, "change:admin remove", this.render)



            //this view listen to the collection remove a user
            // what to do when a user is removed ?
        },

        deleteUser : function(){
            this.collection.remove(this.current);
        },

        template : handlebars.compile(tpl),

        changeUser : function(evt){
            var name = $(evt.target).html();

            console.log("clicked on "+name);

            globalRouter.navigate("users/"+name, {trigger: true});
        },

        render : function(){

            console.log("rendering");

            var data = {
                users :[],
                admins:[],
                current : this.current
            }

            this.collection.each(function(user){
                if (user.isAdmin()){
                    data.admins.push(user)
                }else{
                    data.users.push(user)
                }
            });

            this.$el.html(this.template(data));



            return this;
        },

        setAdmin : function(evt){
            this.current.set({
                admin : true,
                statement : "Hey collection !"
            });
        }

    })

    return UsersView;


});
