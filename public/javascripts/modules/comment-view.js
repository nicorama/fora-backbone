define(["backbone", "handlebars", "text!templates/comment-view.template"], function(Backbone, handlebars, tpl){

    var CommentView = Backbone.View.extend({

        events :{
            "click .like, .dislike" : "like"
        },

        initialize : function(){
            //suppose this.model : comment
        },

        like : function(evt){
            debugger;
            var like = $(evt.target).data("like");
            var score = this.model.get("score");

            if (like){
                this.model.set("score", score++);
            }else{
                this.model.set("score", score--);
            }

            this.render();
            //lire like : true/false
            //modify model.score
            // add class .score-like .score-dislike au comment-view
                // -> render()
            //save model
        },

        template : handlebars.compile(tpl),

        render : function(){
            var data = this.model.toJSON();
            data.score = this.model.get("score")<0 ? "score-dislike" : "score-like";
            this.$el.html( this.template( data) );
            return this;
        }

    })

    return CommentView;

});
