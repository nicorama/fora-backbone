require.config({

    baseUrl: "javascripts/modules",



    paths: {

        "jquery": "../../javascripts/libs/jquery-1.11.1",//not a directory path here. Name must match exactly jquery
        "libs": "../../javascripts/libs",
        "text": "../../javascripts/libs/require-text",
        "underscore" : "../../javascripts/libs/underscore",
        "backbone" : "../../javascripts/libs/backbone",
        "handlebars" : "../../javascripts/libs/handlebars-v2.0.0"
    }

});

var globalRouter ;

require(["model", "users-view"], function (model, UsersView) {

    var collection = new model.UserCollection();

    var AppRouter = Backbone.Router.extend({

        routes : {
            "" : "home",
            "users/:name" : "user"
        },

        home : function(){
            console.log("hello App")
        },

        user : function(name){
            console.log("using User route : "+name)
            collection.fetch({
                success : function(users, data){

                    var current = users.find(function(user){
                        return user.get("name").toLowerCase() == name.toLowerCase();
                    });
                    console.log("found "+current)

                    var view = new UsersView({
                        time : _.now(),
                        el:'body',
                        collection : users,
                        current : current

                    }).render();



                }
            })
        }
    })
    globalRouter = new AppRouter();
    Backbone.history.start();




    var eventBus = _.clone(Backbone.Events)


    var jack = new model.User();
    jack.listenTo(eventBus);



    $.get(url, function(jack){



    })






    ///plus d'entbus

});

