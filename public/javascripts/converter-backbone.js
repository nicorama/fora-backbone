var conversion = [
    [ 1, 1000, 1000000, 0.6213711, 1094, 3280.8399166 ],
    [ 0.001, 1, 1000, 0.0006213, 1.0938712, 3.2804478 ],
    [ 0.000001, 0.001, 1, 0.0000006213, 0.0010938712, 0.0032804478 ],
    [ 1.6093442, 1609.3442, 1609000.3442, 1, 1760.6225973, 5280.0007769 ],
    [ 0.000914, 0.914, 914, 0.0005679, 1, 2.9933802 ],
    [ 0.0003047, 0.3047, 304.7, 0.0001893, 0.3331342, 1 ]
];

var units = [ "km", "m", "mm", "miles", "yards", "feet" ];


var UnitModel = Backbone.Model.extend({

    defaults: {
        units: [ "km", "m", "mm", "miles", "yards", "feet" ],
        result:""
    },

    getResult : function(){
      return this.get("result")
    },

    validate: function (attrs, options) {

        //if we enter some strings
    }

});

var UnitView = Backbone.View.extend({


    events: {
        "click button": "render",
        "change input, #unitInput, #unitOutput": "setModel"
    },

    initialize : function(options){


    },


    template: function () {

        //Use Handlebars



    },

    //classic render function
    render: function () {

        var template = this.template();
        this.$el.html(template);
        return this;
    },


    convert: function (model) {


        var errorText = ""

        var result;
        var idUnite1 = 0;
        var idUnite2 = 0;

        if (!model.get("input")) {
            errorText = "erreur insoutenable";
        } else {
            for (var i = 0; i < units.length; i++) {
                if (model.get("unitInput") == units[i]) {
                    idUnite1 = i;
                }
                if (model.get("unitOutput") == units[i]) {
                    idUnite2 = i;
                }
            }
            result = parseFloat(model.get("input")) * conversion[idUnite1][idUnite2];
            return result;
        }



    }

});


$(document).ready(function () {


    var model = new UnitModel();

    model.on("invalid", function (model, error, evt) {
        alert("invalid data :"+error)
    });

    model.set({
        input: 50,
        unitInput: "yards",
        unitOutput: "m"
    }, {validate: true})


    //create View with no interaction
    //render view
    //create interactions

});

