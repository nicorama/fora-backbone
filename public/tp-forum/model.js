//That can't work with AMD modules ; it could work with browserify
//var Backbone = require("backbone")


var User = Backbone.Model.extend({

    parse:function(obj, options){
        //console.log(options);

        if (!obj.name || obj.name === ''){
            obj.name= 'Anonymous';
        }

        if (obj.name === 'Sheldon'){
            obj.name = 'Shelly';
        }

        return obj;
    },

    url : function(){
        return "/api/users/"+this.get("id")
    },
    toString : function(){
        return this.get("name");
    },

    isAdmin : function(){
        return this.get("admin")===true;
    }
});


var Topic = Backbone.Model.extend({

    url : function(){
        return "/api/topics/"+this.get("id")
    },
    toString : function(){
        return this.get("title");
    }
});


var Comment = Backbone.Model.extend({



    url : function(){
        return "/api/comments/"+this.get("id")
    },
    toString : function(){
        return this.get("content");
    }
});





var TopicCollection = Backbone.Collection.extend({
    model : User,
    url : "users"
});

var CommentCollection = Backbone.Collection.extend({
    model : User,
    url : "users"
});

var UserCollection = Backbone.Collection.extend({
    model : User,
    url : "/api/users"
});




var AdminCollection = new UserCollection();
AdminCollection.parse = function(){
    //stuff
};

